﻿using System.ServiceModel.Channels;

namespace SoapWebServiceCore.InterfaceExternal
{
	public interface IMessageInspector
	{
		object AfterReceiveRequest(ref Message message);
		void BeforeSendReply(ref Message reply, object correlationState);
	}
}

﻿using System.ServiceModel.Channels;
using SoapWebServiceCore.Description;

namespace SoapWebServiceCore.InterfaceExternal
{
	public interface IMessageInspector2
	{
		object AfterReceiveRequest(ref Message message, ServiceDescription serviceDescription);
		void BeforeSendReply(ref Message reply, ServiceDescription serviceDescription, object correlationState);
	}
}

﻿using System;
using System.ServiceModel.Channels;
using SoapWebServiceCore.Enums;

namespace SoapWebServiceCore.Dto
{
	public class SoapOptions
	{
		public Type ServiceType { get; set; }
		public string Path { get; set; }
		public MessageEncoder[] MessageEncoders { get; set; }
		public SoapSerializer SoapSerializer { get; set; }
		public bool CaseInsensitivePath { get; set; }
		public ISoapModelBounder SoapModelBounder { get; set; }
		public Binding Binding { get; set; }
	}
}

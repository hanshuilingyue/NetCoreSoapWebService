﻿using System.Reflection;

namespace SoapWebServiceCore.Dto
{
	public interface ISoapModelBounder
	{
		void OnModelBound(MethodInfo methodInfo, object[] prms);
	}
}

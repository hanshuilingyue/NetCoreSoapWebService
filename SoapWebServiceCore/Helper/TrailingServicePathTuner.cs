﻿using Microsoft.AspNetCore.Http;

namespace SoapWebServiceCore.Helper
{
	/// <summary>
	/// This tuner truncates the incoming http request to the last path-part. ie. /DynamicPath/Service.svc becomes /Service.svc
	/// Register this tuner in ConfigureServices: services.AddSoapServiceOperationTuner(new TrailingServicePathTuner());
	/// </summary>
	public class TrailingServicePathTuner
	{
		public void ConvertPath(HttpContext httpContext)
		{
			string trailingPath = httpContext.Request.Path.Value.Substring(httpContext.Request.Path.Value.LastIndexOf('/'));
			httpContext.Request.Path = new PathString(trailingPath);
		}
	}
}

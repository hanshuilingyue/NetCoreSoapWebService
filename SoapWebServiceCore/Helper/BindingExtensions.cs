﻿using System.Net;
using System.ServiceModel.Channels;

namespace SoapWebServiceCore.Helper
{
	internal static class BindingExtensions
	{
		public static bool HasBasicAuth(this Binding binding)
		{
			if (binding == null)
			{
				return false;
			}

			return binding.CreateBindingElements().Find<HttpTransportBindingElement>().AuthenticationScheme == AuthenticationSchemes.Basic;
		}
	}
}

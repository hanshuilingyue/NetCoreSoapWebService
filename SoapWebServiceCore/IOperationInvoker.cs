﻿using System.Reflection;
using System.Threading.Tasks;

namespace SoapWebServiceCore
{
	public interface IOperationInvoker
	{
		Task<object> InvokeAsync(MethodInfo methodInfo, object instance, object[] inputs);
	}
}

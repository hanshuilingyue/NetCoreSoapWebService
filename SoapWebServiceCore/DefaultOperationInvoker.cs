﻿using System.Reflection;
using System.Threading.Tasks;

namespace SoapWebServiceCore
{
	public class DefaultOperationInvoker : IOperationInvoker
	{
		public async Task<object> InvokeAsync(MethodInfo methodInfo, object serviceInstance, object[] arguments)
		{
			// Invoke Operation method
			var responseObject = methodInfo.Invoke(serviceInstance, arguments);
			if (methodInfo.ReturnType.IsConstructedGenericType && methodInfo.ReturnType.GetGenericTypeDefinition() == typeof(Task<>))
			{
				var responseTask = (Task)responseObject;
				await responseTask;
				responseObject = responseTask.GetType().GetProperty("Result").GetValue(responseTask);
			}
			else if (responseObject is Task responseTask)
			{
				await responseTask;

				//VoidTaskResult
				responseObject = null;
			}

			return responseObject;
		}
	}
}

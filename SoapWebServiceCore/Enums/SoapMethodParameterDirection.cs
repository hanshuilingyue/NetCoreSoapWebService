﻿namespace SoapWebServiceCore.Enums
{
	public enum SoapMethodParameterDirection
	{
		InOnly,
		OutOnlyRef,
		InAndOutRef
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;

namespace NetCoreSoapWebService.WebService
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceContract(Name = "TestServiceSoap")]
    public interface ITestServiceSoap
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input1"></param>
        /// <param name="input2"></param>
        /// <returns></returns>
        [OperationContract(Name = "GetTestSoap")]
        List<string> GetTestSoap(int input1, string input2);

    }

    /// <summary>
    /// 
    /// </summary>
    public class TestServiceSoap : ITestServiceSoap
    {

        /// <summary>
        /// 
        /// </summary>
        public TestServiceSoap()
        {
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input1"></param>
        /// <param name="input2"></param>
        /// <returns></returns>
        public List<string> GetTestSoap(int input1, string input2)
        {
            return new List<string> { "123", "345", "56789" };
        }

    }

}
